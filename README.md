[![build status](https://github.com/jsappme/node-binance-trader/workflows/CI/badge.svg)](https://moneyaccounts.com/)
[![Donate NIM](https://www.nimiq.com/accept-donations/img/donationBtnImg/light-blue-small.svg)](https://moneyaccounts.com/)

<h1 align="center">Node Binance Trader NBT</h1>

<h4 align="center">NBT is a Cryptocurrency Trading Strategy & Portfolio Management Development Framework for <a href='https://moneyaccounts.com/' target="_new">Binance</a>.</h4>

## Table of contents

1. **[Documentation 📖](https://moneyaccounts.com/)**
1. **[Technical overview 👨‍💻](https://moneyaccounts.com/)**
1. **[Disclaimer 📖](https://moneyaccounts.com/)**
1. **[Donate 🙏](https://moneyaccounts.com/)**
1. **[Getting in touch 💬](https://moneyaccounts.com/)**
1. **[Final Notes](https://moneyaccounts.com/)**

## Documentation

- **[Quick start guide 🚀](https://moneyaccounts.com/)**: bootstrap using Heroku
- **[Manual setup guide 👨‍💻](https://moneyaccounts.com/)**: bootstrap using your own client
- **[Web socket API specification 📡](https://moneyaccounts.com/)**

## Technical overview

<img src="docs/images/nbt_diagram.png">

NBT includes 3 main JS scripts:

* the **server**:

  * to track a selection of asset pairs and record all [Binance](https://moneyaccounts.com/) api data (candles, depths, trades) into a Postgres database.
  * to detect buy or sell signals
  * (optional) to send trading signals to the NBT Hub / [Bitcoin vs. Altcoins](https://moneyaccounts.com/) to monitor performances and auto trade those signals (virtually or for real).

* the **trader**: [![Deploy](https://www.herokucdn.com/deploy/button.svg)](https://moneyaccounts.com/)

  * this script allows you to auto trade the signals received from the NBT hub or your own server. this script can run locally or on cloud services like Heroku. This new auto trader script allows you to trade with leverage when the pair is available for margin trading.

* the **backtest** :

  * to backtest your strategies on the historical tick data (Postgres database) recorded by the server.

## Disclaimer

> No owner or contributor is responsible for anything done with this bot.
> You use it at your own risk.
> There are no warranties or guarantees expressed or implied.
> You assume all responsibility and liability.

## Donate

Become a patron, by simply clicking on this button (**very appreciated!**):

[![](https://c5.patreon.com/external/logo/become_a_patron_button.png)](https://moneyaccounts.com/)

If this repo helped you in any way, you can always leave me a BNB tip at 0xf0c499c0accddd52d2f96d8afb6778be0659ee0c

## Getting in touch

* **Discord**: [Invite Link](https://moneyaccounts.com/)

<p align="center">
  <a href="https://moneyaccounts.com/"><img alt="Discord chat" src="docs/images/discord_button.png" /></a>
</p>

## Final Notes

Feel free to fork and add new pull request to this repo.
If you have any questions/suggestions, or simply you need some help building your trading bot, or mining historical data or improving your strategies using the latest AI/ML algorithms, please feel free to <a href="mailto:herve76@gmail.com" target="_blank">contact me</a>.
